/* Copyright (c) 1998 Lucent Technologies - All rights reserved. */
#include <u.h>
#include <libc.h>
#include <libg.h>
#include <frame.h>

static int tabwidth() {
	char *t=getenv("tabstop");
	int tab=8;
	
	if(t) {
		tab=0;
		while(*t) {
			tab = 10*tab + *t-'0'; 
			t++;
		}
	}
	if(tab<1)
		tab = 1;
	else if(tab>8)
		tab = 8;
	return tab;
}

void
frinit(Frame *f, Rectangle r, Font *ft, Bitmap *b)
{
	f->font = ft;
	f->maxtab = tabwidth()*charwidth(ft, '0');
	f->nbox = 0;
	f->nalloc = 0;
	f->nchars = 0;
	f->nlines = 0;
	f->p0 = 0;
	f->p1 = 0;
	f->box = 0;
	f->lastlinefull = 0;
	frsetrects(f, r, b);
}

void
frsetrects(Frame *f, Rectangle r, Bitmap *b)
{
	f->b = b;
	f->entire = r;
	f->r = r;
	f->r.max.y -= (r.max.y-r.min.y)%f->font->height;
	f->left = r.min.x+1;
	f->maxlines = (r.max.y-r.min.y)/f->font->height;
}

void
frclear(Frame *f)
{
	if(f->nbox)
		_frdelbox(f, 0, f->nbox-1);
	if(f->box)
		free(f->box);
	f->box = 0;
}
