//
//  main.c
//  testapp
//
//  Created by David Tolpin on 31.12.04.
//  Copyright Davidashen 2004, 2005. All rights reserved.
//

#define Cursor cCursor

#include <CoreFoundation/CoreFoundation.h>
#include <Carbon/Carbon.h>

#undef Cursor

static void say_hello(WindowRef window);
static void store_window_bounds(WindowRef window);
static void recall_window_bounds(Rect *rp);
static Fixed font_size();
static void install_handlers(WindowRef window);

int main(int argc, char* argv[])
{
	WindowRef 		window;
	Rect			wrect;
	OSStatus		err;

	recall_window_bounds(&wrect);
	err = CreateNewWindow(
		kDocumentWindowClass,
		kWindowStandardDocumentAttributes|kWindowLiveResizeAttribute,
		&wrect,
		&window);
	require_noerr(err,CantCreateWindow);
	SetPortWindowPort(window);
	install_handlers(window);
	SelectWindow(window);
	ShowWindow(window);

	RunApplicationEventLoop();
	return 0;

CantCreateWindow:
	fprintf(stderr,"error %d\n",err);
	return err;
}

static OSStatus
quit_app()
{
	EventRef quit;
	CreateEvent(NULL,kEventClassApplication,kEventAppQuit,
		0,kEventAttributeUserEvent,&quit);
	SendEventToEventTarget(quit,GetApplicationEventTarget());
}

static OSStatus
on_window(EventHandlerCallRef next,EventRef event,void *data)
{
	switch(GetEventKind(event)) {
	case kEventWindowBoundsChanged:
		store_window_bounds((WindowRef)data);
		say_hello((WindowRef)data);
		break;
	case kEventWindowClosed:
		quit_app();
		break;
	case kEventWindowDrawContent:
		say_hello((WindowRef)data);
		break;
	default:
		assert(0);
	}
	CallNextEventHandler(next,event);
}

static OSStatus
on_app(EventHandlerCallRef next,EventRef event,void *data)
{
	switch(GetEventKind(event)) {
	case kEventAppQuit:
		fprintf(stderr,"не поминайте лихом\n");
		break;
	default:
		assert(0);
	}
	CallNextEventHandler(next,event);
}

static void
install_handlers(WindowRef window)
{
	InstallStandardEventHandler(GetWindowEventTarget(window));
	{
		EventTypeSpec evtypes[] = {
			{kEventClassWindow,kEventWindowBoundsChanged},
			{kEventClassWindow,kEventWindowClosed},
			{kEventClassWindow,kEventWindowDrawContent},
		};
		EventHandlerRef handler;
		InstallEventHandler(
			GetWindowEventTarget(window),
			NewEventHandlerUPP(on_window),
			3, evtypes, window, &handler);
	}
	{
		EventTypeSpec evtypes[] = {
			{kEventClassApplication,kEventAppQuit}
		};
		EventHandlerRef handler;
		InstallEventHandler(
			GetApplicationEventTarget(),
			NewEventHandlerUPP(on_app),
			1, evtypes, NULL, &handler);
	}
}

#define recall_coord(name) \
	val = CFPreferencesCopyAppValue(CFSTR("win-"#name), \
		kCFPreferencesCurrentApplication); \
	if(val) { \
		CFNumberGetValue(val,kCFNumberShortType,&rp->name); \
		CFRelease(val); \
	}

static void
recall_window_bounds(Rect *rp)
{
	CFNumberRef val;
	
	SetRect(rp,50,50,700,600);
	recall_coord(left); recall_coord(top);
	recall_coord(right); recall_coord(bottom);
}

#define store_coord(name) \
	val = CFNumberCreate(kCFAllocatorDefault,kCFNumberShortType,&rect.name); \
	CFPreferencesSetAppValue(CFSTR("win-"#name),val, \
		kCFPreferencesCurrentApplication); \
	CFRelease(val);

static void
store_window_bounds(WindowRef window)
{
	Rect rect;
	CFNumberRef val;

	GetWindowBounds(window,kWindowContentRgn,&rect);
	store_coord(left); store_coord(top);
	store_coord(right); store_coord(bottom);
	CFPreferencesAppSynchronize(kCFPreferencesCurrentApplication);
}

static Fixed
font_size(void)
{
	short sz = 13; /* default font size */
	CFStringRef key = CFSTR("font-size");
	CFNumberRef val = CFPreferencesCopyAppValue(key,
		kCFPreferencesCurrentApplication);

	if(val) {
		CFNumberGetValue(val,kCFNumberShortType,&sz);
	} else {
		val = CFNumberCreate(kCFAllocatorDefault,kCFNumberShortType,&sz);
		CFPreferencesSetAppValue(key,val,
			kCFPreferencesCurrentApplication);
	}
	CFRelease(val);
	return IntToFixed(sz);
}

static ATSUFontID
font_family(void)
{
	ATSUFontID fontid;
	CFStringRef family = CFSTR("Monaco"); /* default font */
	CFStringRef key = CFSTR("font-family");
	CFStringRef val = CFPreferencesCopyAppValue(key,
		kCFPreferencesCurrentApplication);

	if(val) {
		family = val;
	} else {
		val = family;
		CFRetain(val);
		CFPreferencesSetAppValue(key,val,
			kCFPreferencesCurrentApplication);
	}
	fontid = ATSFontFindFromName(family,kATSOptionFlagsDefault);
	CFRelease(val);
	return fontid;
}

static ATSUStyle
text_style()
{
	static ATSUStyle style = NULL;
	if(style==NULL) {
		ATSUAttributeTag  tags[] =  {kATSUSizeTag,kATSUFontTag};
		ByteCount sizes[] = {sizeof(Fixed),sizeof(ATSUFontID)};
		Fixed size = font_size();
		Fixed font = font_family();
		ATSUAttributeValuePtr values[] = {&size,&font};

		ATSUCreateStyle(&style);
		ATSUSetAttributes (style,2,tags,sizes,values);

		/* get font name */
		{
			ATSUFontID fontid; ByteCount vs;
			ATSFontRef fontref;
			CFStringRef fontname; char buffer[256];
			
			ATSUGetAttribute(text_style(),kATSUFontTag,
				sizeof(ATSUFontID),&fontid,&vs);
			fontref = FMGetATSFontRefFromFont(fontid);
			ATSFontGetName(fontref,kATSOptionFlagsDefault,&fontname);
			CFStringGetCString(fontname,buffer,256,kCFStringEncodingUTF8);
			fprintf(stderr,"%s\n",buffer);
			CFRelease(fontname);
		}
	}
	return style;
}

static void
say_hello(WindowRef window)
{
	ATSUTextLayout  textLayout;
	CGContextRef	context;
	UniChar text[] = {'H','e','l','l','o',' ','S','a','m','!'};
	Rect wrect,lrect;
	
	GetWindowBounds(window,kWindowStructureRgn,&wrect);
	
	SetRect(&lrect,0,0,wrect.right-wrect.left,wrect.bottom-wrect.top);		
	EraseRect(&lrect);

	ATSUCreateTextLayout(&textLayout);
	ATSUSetTextPointerLocation(textLayout, text,
		kATSUFromTextBeginning, kATSUToTextEnd,
			sizeof(text)/sizeof(UniChar));

	ATSUSetRunStyle(textLayout,text_style(),
		kATSUFromTextBeginning,kATSUToTextEnd);
		
	QDBeginCGContext(GetWindowPort(window),&context);
	{
		ATSUAttributeTag        tags[] = {kATSUCGContextTag};
		ByteCount               sizes[] = {sizeof (CGContextRef)};
		ATSUAttributeValuePtr   values[] = {&context};
		ATSUSetLayoutControls (textLayout, 1,tags,sizes,values);
	}

	{ /* draw centered text */
		Rect trect;
		ATSUMeasureTextImage(textLayout,
			kATSUFromTextBeginning,kATSUToTextEnd,
			IntToFixed((wrect.right-wrect.left)/2),
			IntToFixed((wrect.bottom-wrect.top)/2),
			&trect);
		ATSUDrawText(textLayout,kATSUFromTextBeginning,kATSUToTextEnd,
			IntToFixed((wrect.right-wrect.left)/2-(trect.right-trect.left)/2),
			IntToFixed((wrect.bottom-wrect.top)/2-(trect.bottom-trect.top)/2));
	}
	
	CGContextFlush(context);
	QDEndCGContext(GetWindowPort(window),&context);
}
